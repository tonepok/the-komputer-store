const loanElement = document.getElementById("loanButton");
const repayElement = document.getElementById("repayButton");
const bankElement = document.getElementById("bankButton");
const oLoanElement = document.getElementById("oLoan");
const workElement = document.getElementById("workButton");
const buyElement = document.getElementById("buyButton");
const balanceElement = document.getElementById("balance");
const payElement = document.getElementById("pay");
const imgElement = document.getElementById("photo");
const laptopListElement = document.getElementById("laptopList");
const specsElement = document.getElementById("specs");
const laptopNameElement = document.getElementById("laptopName");
const descElement = document.getElementById("desc");
const stockElement = document.getElementById("stock");
const toPayElement = document.getElementById("toPay");
let laptops = [];

const main = async () => {
    await getAPI();
    selectLaptop();
    laptopListElement.addEventListener("change", selectLaptop);
}
main();

//changes the info that is showed based on what the user selects
//in the drop-down
const selectLaptop = () => {
    let index = laptopListElement.value;
    let laptop = laptops[index-1];
    descElement.innerHTML = laptop.description;
    laptopNameElement.innerHTML = laptop.title;
    let prettySpecs = laptop.specs[0];
    for (let i=1; i<laptop.specs.length; i++){
        prettySpecs+='<br>';
        prettySpecs+=laptop.specs[i];
    }
    specsElement.innerHTML = prettySpecs;
    let priceSentence = laptop.price + " kr";
    toPayElement.innerHTML = priceSentence;
    //changes the info and color of stock info
    if (laptop.stock > 0){
        stockElement.innerHTML = "Stock: "+laptop.stock;
        stockElement.style.color = 'black';
    }
    else{
        stockElement.innerHTML = "Out of stock!";
        stockElement.style.color = 'red';
    }
    /*I do not know how to access the images, so added some random in folder
    let imgLink = 'https://noroff-komputer-store-api.herokuapp.com/computers';
    imgLink+= laptop.image;
    imgElement.src = imgLink;*/
    imgElement.src = laptop.image;
    //imgElement.src = "https://th.bing.com/th/id/R.cb4ca329ec84faa13451157cb75a8264?rik=1xNrkGYNpA8J0g&pid=ImgRaw&r=0";
}

//enabled by the buy button. Will buy and subtract the price if success,
//otherwise it will show alert with why its not working
const buy = () => {
    let balanceInt = getInt(balanceElement.firstChild.nodeValue);
    const liste = toPayElement.firstChild.nodeValue.split(' ');
    let payInt = +liste[0];
    let laptop = laptops[(laptopListElement.value)-1];
    if (balanceInt < payInt){ //if not enough in bank
        alert("Sorry, you don't have enough money to buy this!");
    }
    else if(laptop.stock < 1){
        alert("Sorry, out of stock!"); 
    }
    else {
        alert("Congrats! You bought a "+laptopNameElement.firstChild.nodeValue+"!"); 
        let newBalance = balanceInt-payInt;
        balanceElement.firstChild.nodeValue = "Balance: "+ newBalance +" kr";
        laptop.stock--;
        selectLaptop((laptopListElement.value)-1); //refresh stock info
    }
}

//increases by 100 if button is pressed
const work = () => {
    let newMoney = getInt(payElement.firstChild.nodeValue) + 100;
    payElement.firstChild.nodeValue = "Pay: "+newMoney+" kr";
}

//gets a loan from bank if currency is >0 and dont already have a loan.
//Unclear instructions about if its ok to get another loan after
//the user has bought a computer... Because it still sais that one
//needs to pay it back before getting another loan
const getLoan = () => {
    let currentMoney = getInt(balanceElement.firstChild.nodeValue);
    const liste = oLoanElement.firstChild.nodeValue.split(' ');
    let loanMoney = +liste[2];
    if (currentMoney <= 0){
        alert("You can not loan.");
    }
    else if (loanMoney != 0){ 
        alert("Good try u fool. Buy a computer or pay back first!");
    }
    else{
        var loanAmount = prompt("How much do you want to loan?");
        if (loanAmount != parseInt(loanAmount, 10)){
            alert("You need to pass a number.");
        }
        else if (currentMoney*2 < loanAmount){
            alert("You can not loan that much.");
        }
        else {
            document.getElementById("repayButton").style.visibility = "visible";
            document.getElementById("oLoan").style.visibility = "visible";
            oLoanElement.firstChild.nodeValue = "Outstanding loan: "+loanAmount+" kr";

            const currentBalance = balanceElement.firstChild.nodeValue;
            let newMoney = getInt(currentBalance) + parseInt(loanAmount);
            balanceElement.firstChild.nodeValue = "Balance: "+newMoney+" kr";
        }
    }
}

//repays money from work-section. If not enough for everything, it will
//pay back as much as possible. 
const repayLoan = () => {
    let loanS = oLoanElement.firstChild.nodeValue;
    const liste = loanS.split(' ');
    let loanMoney = +liste[2];
    let payMoney = getInt(payElement.firstChild.nodeValue);
    let newMoney = payMoney - loanMoney;
    if (payMoney == 0){
        alert("You need to work first!");
    }
    else if (newMoney > 0){ //if still more Pay
        payElement.firstChild.nodeValue = "Pay: "+ newMoney +" kr";
        document.getElementById("repayButton").style.visibility = "hidden";
        document.getElementById("oLoan").style.visibility = "hidden";
        oLoanElement.firstChild.nodeValue = "Outstanding loan: "+0+" kr";

        const currentBalance = balanceElement.firstChild.nodeValue;
        let newB = getInt(currentBalance) - loanMoney;
        balanceElement.firstChild.nodeValue = "Balance: "+newB+" kr";
    }
    else { //not enough for everything, but some
        payElement.firstChild.nodeValue = "Pay: "+ 0 +" kr";
        let newLoan = loanMoney - payMoney; 
        oLoanElement.firstChild.nodeValue = "Outstanding loan: "+newLoan+" kr";
        const currentBalance = balanceElement.firstChild.nodeValue;
        let newB = getInt(currentBalance) - payMoney;
        balanceElement.firstChild.nodeValue = "Balance: "+newB+" kr";
    }
}

//put money in bank. Everything if no loan, if loan then 10% goes to
//pay down loan. 
const bank = () => {
    let payMoney = getInt(payElement.firstChild.nodeValue);
    payElement.firstChild.nodeValue = "Pay: "+ 0 +" kr";

    const liste = oLoanElement.firstChild.nodeValue.split(' ');
    let loanMoney = +liste[2];
    if (loanMoney <= 0){
        const currentBalance = balanceElement.firstChild.nodeValue;
        let newMoney = getInt(currentBalance) + payMoney;
        balanceElement.firstChild.nodeValue = "Balance: "+newMoney+" kr";
    }
    else {
        const tenPercent = 10/payMoney*100;
        const afterPayback = loanMoney-tenPercent;
        let rest = payMoney-tenPercent;
        const currentBalance = balanceElement.firstChild.nodeValue;
        let newMoney = getInt(currentBalance) + rest;
        balanceElement.firstChild.nodeValue = "Balance: "+newMoney+" kr";
        oLoanElement.firstChild.nodeValue = "Outstanding loan: "+afterPayback+" kr";
    }
}

//just a little helper method
const getInt = (sentence) => +sentence.split(' ')[1];

async function getAPI(){
// const getAPI = async () => {     Why is this not working?
    const addLaptops = (laptops) => {
        laptops.forEach(x => addLaptop(x));
    }
    const addLaptop = (laptop) => {
        const laptopElement = document.createElement("option");
        laptopElement.value = laptop.id;
        laptopElement.appendChild(document.createTextNode(laptop.title));
        laptopListElement.appendChild(laptopElement);

    }
    await fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptops(laptops))
    .catch(error => {
        console.error("Error", error);
    })
}

//adding event listeners
loanElement.addEventListener("click", getLoan);
repayElement.addEventListener("click", repayLoan);
bankElement.addEventListener("click", bank);
workElement.addEventListener("click", work);
buyElement.addEventListener("click", buy);